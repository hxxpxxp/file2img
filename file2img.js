var file_bytes=null;
var length;
var width,height;
var num_of_pixels; 
var max_num_of_pixels;
var bytes_per_pixel;
//following both are td elements
var first_selected_pixel=null;
var last_selected_pixel=null;
function pixel_to_byte_index_range(i){
	let res=new Object();
	//[] kinda range
	res["start"]=Math.ceil((i)*bytes_per_pixel);
	res["end"]=Math.ceil((i+1)*bytes_per_pixel)-1;
	return res;
}
//returns null when byte index goes out of the file
function calculate_pixel_value(i){
	//[] kinda  range
	let range=pixel_to_byte_index_range(i);
	let start_byte_index=range["start"];
	let last_byte_index=range["end"];
	if(last_byte_index>=length)return null;
	let byte=0;
	if(start_byte_index!=last_byte_index){ //average out the bytes
		for(let index=start_byte_index;index<=last_byte_index;index++){
			byte+=file_bytes[index];
		}
		byte=Math.ceil(byte/(last_byte_index-start_byte_index+1));
	}else{
		byte=file_bytes[start_byte_index];
	}
	return byte;
}
function make_img(){
	if(file_bytes == null)return;
	clear_selection();
	length=file_bytes.length;
	if(length>max_num_of_pixels){
		num_of_pixels=max_num_of_pixels;
		bytes_per_pixel=length/num_of_pixels;
	}else{
		num_of_pixels=length;
		bytes_per_pixel=1;
	}
	document.querySelector("#bytes_per_pixel").innerText=""+bytes_per_pixel;
	width=parseInt(document.querySelector("#width").value);
	if(width==0)width=Math.ceil(Math.sqrt(num_of_pixels));
	height=Math.ceil(num_of_pixels/width);
	console.log(length);
	console.log(num_of_pixels);
	console.log(bytes_per_pixel);
	console.log(width);
	console.log(height);
	let table=document.createElement("table");
	for(let i=0;i<height;i++){
		let tr=document.createElement("tr");
		for(let j=0;j<width;j++){
			let byte=calculate_pixel_value(i*width+j);
			if(byte==null)break;
			let td=document.createElement("td");
			td.style.backgroundColor="rgb("+byte+","+byte+","+byte+")";
			tr.appendChild(td);
		}
		table.appendChild(tr);
	}
	let img_container=document.querySelector("#img_container");
	img_container.replaceChildren();//this empties the div
	img_container.appendChild(table);
	
}
function loadFile(file){
	document.querySelector("#filename").textContent=file.name;
	file.arrayBuffer().then(buffer=>{
		const bytes=new Uint8Array(buffer);
		file_bytes=bytes;
		make_img();
	});
}
//loads code from the dropped file
function loadDroppedFile(ev){
  	ev.preventDefault();
  	ev.stopPropagation();
	let numOfFiles=ev.dataTransfer.files.length;
	if(numOfFiles==0)return;
	loadFile(ev.dataTransfer.files[0]);

}
//loads code from the opened file
function openFile(ev){
	loadFile(ev.currentTarget.files[0]);
}
//update the css that affects the pixel size of the image
function updatePixelSize(){
	let pixel_size=document.querySelector("#pixel_size").value;
	document.querySelector("#pixel_size_css").innerHTML="#img_container table td{width:"+pixel_size+"px;height:"+pixel_size+"px;}";
}
//update max_num_of_pixels
function update_max_num_of_pixels(){
	max_num_of_pixels=document.querySelector("#max_num_of_pixels").value;
	make_img();
}
function find_pixel_num_of_td(td){
	return (td.parentNode.rowIndex*width)+td.cellIndex;
}
function find_td_of_pixel_num(i){
	let table=document.querySelector("#img_container table");
	return table.children[Math.floor(i/width)].children[i%width];
}
//just removes the selection color from the img
function make_it_original(){
	if(first_selected_pixel!=null){
		if(last_selected_pixel!=null){
			let first_pixel_num=find_pixel_num_of_td(first_selected_pixel);
			let last_pixel_num=find_pixel_num_of_td(last_selected_pixel);
			//to handle reverse selection
			if(last_pixel_num<first_pixel_num){
				let tmp=last_pixel_num;
				last_pixel_num=first_pixel_num;
				first_pixel_num=tmp;
			}
			for(let i=first_pixel_num;i<=last_pixel_num;i++){
				let byte=calculate_pixel_value(i);
				find_td_of_pixel_num(i).style.backgroundColor="rgb("+byte+","+byte+","+byte+")";
			}
		}else{
			let byte=calculate_pixel_value(find_pixel_num_of_td(first_selected_pixel));
			first_selected_pixel.style.backgroundColor="rgb("+byte+","+byte+","+byte+")";
		}
	}
}
function clear_selection(){
	make_it_original();
	first_selected_pixel=null;
	last_selected_pixel=null;
	document.querySelector("#selection_range").textContent="NA";
}
function update_selection_range(){
	let first_byte_index=null,last_byte_index=null;
	let text="NA";
	if(first_selected_pixel!=null){
		let range=pixel_to_byte_index_range(find_pixel_num_of_td(first_selected_pixel));
		first_byte_index=range["start"];
		last_byte_index=range["end"];
		if(last_selected_pixel!=null){
			let range=pixel_to_byte_index_range(find_pixel_num_of_td(last_selected_pixel));
			if(range["start"]<last_byte_index){//reverse selection
				first_byte_index=range["start"];
			}else{
				last_byte_index=range["end"];
			}
		}
		if(first_byte_index==last_byte_index){
			text=first_byte_index+" (0x"+first_byte_index.toString(16)+")";
		}else{
			text=first_byte_index+"-"+last_byte_index+" (0x"+first_byte_index.toString(16)+"-0x"+last_byte_index.toString(16)+")";
		}
	}
	document.querySelector("#selection_range").textContent=text;
}
function select_pixel(ev){
	if(!(ev.target.tagName=="TD"))return;
	make_it_original();
	if(ev.shiftKey && first_selected_pixel && ev.target!=first_selected_pixel){
		if(last_selected_pixel!=null){
			first_selected_pixel=last_selected_pixel;
			last_selected_pixel=null;
		}
		last_selected_pixel=ev.target;
		let first_pixel_num=find_pixel_num_of_td(first_selected_pixel);
		let last_pixel_num=find_pixel_num_of_td(last_selected_pixel);
		//to handle reverse selection
		if(last_pixel_num<first_pixel_num){
			let tmp=last_pixel_num;
			last_pixel_num=first_pixel_num;
			first_pixel_num=tmp;
		}
		for(let i=first_pixel_num;i<=last_pixel_num;i++){
			//let byte=calculate_pixel_value(i);
			find_td_of_pixel_num(i).style.backgroundColor="rgb(255,0,0)";
		}
		
	}else{
		let td=ev.target;
		td.style.backgroundColor="rgb(255,0,0)";
		first_selected_pixel=td;
		last_selected_pixel=null;
	}
	update_selection_range();
}
//do all the initalization stuff
window.onload=()=>{
	document.querySelector("#open_file_btn").addEventListener("click",()=>document.querySelector("#open_file").click());
	document.querySelector("#open_file").onchange=openFile;
	document.querySelector("#width").onchange=make_img;
	document.querySelector("#pixel_size").onchange=updatePixelSize;
	updatePixelSize();
	document.querySelector("#max_num_of_pixels").onchange=update_max_num_of_pixels;
	update_max_num_of_pixels();
	document.querySelector("#img_container").addEventListener("click",select_pixel);
	//handle dropping of files
	window.addEventListener("dragover",e=>e.preventDefault(),true);
	window.addEventListener("drop",loadDroppedFile,true);
};
